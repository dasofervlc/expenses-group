# expenses group

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Know defects](#Know-defects)
* [Things to improve](#Things-to-improve)


## General info
The main objective of this rest Microservices is received 
and retrieve the updates of expenses group. 
* Develop in Kotlin
* Implemented a Spring application using DDD.
* Organized layers with the help of Hexagonal Architecture.
* Added Swagger UI that allow us to interact with our API specification and exercise the endpoints.
* Postgres database build in dockers with sh to run more easy
* Dockerfile and sh to run the application in dockers

## Technologies
Project is created with:
* Kotlin
* Spring boot
* Reactive programing
* R2dbc
* Postgres
* Swagger
* Maven
* Docker-compose

## :computer: How tu use
* Start Database:
```
$ docker-compose up -d
```
* Build project in local:
```
$ mvn clean install
```
* Build project in dockers:
```
$ mvn clean package
$ ./AppRunner.sh
```
* Access to swagger-ui:
```
http://localhost:8080/swagger-ui/index.html
```

* Access to api-docs:
```
http://localhost:8080/v3/api-docs/
```
## :memo: Know defects
- app jar is not finding the kotlin main class when do the java -jar
- Pending save payment validate Person exist
- mvn test is not picking the tests
- Spring initializr kotlin jar is not working properly with intelliJ

## :pushpin: Things to improve

- Improve code to manage events 
- Create database unit test using H2, improve the coverage testing and add Integration test starting from a mockServer
- Improve the logic to add persons, update or delete payments
- Improve application logging and exception handle