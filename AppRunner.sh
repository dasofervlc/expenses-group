#!/bin/sh
if [ !"$(docker images -q expenses-group-image:latest 2>/dev/null)" == "" ]; then
  docker image rm expenses-group-image
  fi
docker build -t expenses-group-image .
docker run --name expenses-group-container -p 8080:8080 expenses-group-image
docker container start expenses-group-container