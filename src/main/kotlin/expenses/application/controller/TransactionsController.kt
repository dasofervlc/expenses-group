package expenses.application.controller

import com.fasterxml.jackson.databind.ObjectMapper
import expenses.domain.model.Payment
import expenses.domain.model.Payment_data
import expenses.domain.port.application.TransactionsServicePort
import io.swagger.v3.oas.annotations.tags.Tag
import mu.KotlinLogging
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import javax.validation.constraints.NotBlank


@RestController
@Tag(name = "Transactions")
class TransactionsController(private var transactionsServicePort: TransactionsServicePort) {

    private val logger = KotlinLogging.logger {}


    @PostMapping("/payment", consumes = [MediaType.APPLICATION_JSON_VALUE])
    @ResponseBody
    fun addPayment(@RequestBody @NotBlank(message = "Payment fields cannot be empty") payment: Payment):
            Mono<Payment_data> {
        logger.info { "Payment received: " + ObjectMapper().writeValueAsString(payment) }
        return transactionsServicePort.addPayment(payment)
            .doOnError { error -> logger.error("Error during payment transaction", error) }
    }

}