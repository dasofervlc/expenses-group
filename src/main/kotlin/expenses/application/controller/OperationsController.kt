package expenses.application.controller

import expenses.domain.model.PersonAssets_data
import expenses.domain.port.application.OperationsServicePort
import io.swagger.v3.oas.annotations.tags.Tag
import mu.KotlinLogging
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono


@RestController
@Tag(name = "Operations")
class OperationsController(private var operationsServicePort: OperationsServicePort) {

    private val logger = KotlinLogging.logger {}


    @GetMapping("/TotalBalance", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun getExpensesGroup(): Mono<List<PersonAssets_data>> {

        return operationsServicePort.getTotalBalance()
            .doOnError { error -> logger.error("Error during balances retrieval", error) }
    }

}