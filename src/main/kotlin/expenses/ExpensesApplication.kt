package expenses

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class ExpensesApplication

fun main(args: Array<String>) {
    try {
        runApplication<ExpensesApplication>(*args)
    }catch (e:Exception) {
        e.printStackTrace();
    }
}