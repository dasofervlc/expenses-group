package expenses.domain.services

import expenses.domain.exceptions.TransactionNotPersistedException
import expenses.domain.exceptions.UnexpectedException
import expenses.domain.model.Payment
import expenses.domain.model.Payment_data
import expenses.domain.port.application.TransactionsServicePort
import expenses.domain.port.infrastructure.TransactionsRepository
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono

open class TransactionsServiceImpl(private var transactionRepository: TransactionsRepository) : TransactionsServicePort {

    @Transactional
    override fun addPayment(payment: Payment): Mono<Payment_data> {

        return transactionRepository.save(createPaymentRecordDto(payment))
            .doOnError { throw  TransactionNotPersistedException("Payment cannot be persisted") }

    }

    private fun createPaymentRecordDto(payment: Payment): Payment_data {
        var actualSerial =0
        getSerial().doOnSuccess { x: Int -> actualSerial = x }
        return Payment_data(
                actualSerial,
                payment.name,
                payment.description,
                payment.payment,
                payment.date.toLocalDate()
            )
    }

    open fun getSerial(): Mono<Int> {

        return transactionRepository.getSerial()
            .doOnError { throw  UnexpectedException("Serial cannot be retrieved") }
    }
}