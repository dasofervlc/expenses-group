package expenses.domain.services

import expenses.domain.model.PersonAssets_data
import expenses.domain.port.application.OperationsServicePort
import expenses.domain.port.infrastructure.OperationsRepository
import mu.KotlinLogging
import reactor.core.publisher.Mono

class OperationsServiceImpl(var operationsRepository: OperationsRepository) : OperationsServicePort {

    private val logger = KotlinLogging.logger {}

    override fun getTotalBalance(): Mono<List<PersonAssets_data>> {
        logger.info { "Query to retrieve total balance" }
        return operationsRepository.getTotalBalance()
            .doOnError { error -> logger.error("Not data returned form BALANCE table", error) }

    }
}