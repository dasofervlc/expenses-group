package expenses.domain.mappers

import expenses.domain.exceptions.UnexpectedException
import expenses.domain.model.Payment_data
import io.r2dbc.spi.Row
import org.springframework.stereotype.Component
import java.time.LocalDate
import java.util.function.BiFunction


@Component
class PaymentDataMapper: BiFunction<Row, Any, Payment_data> {
    override fun apply(row: Row, o: Any): Payment_data {
        return Payment_data(
            row.get("PAYMENT_ID", Number::class.java)?.toInt() ?: throw UnexpectedException(),
            row.get("USER_NAME", String::class.java) ?: throw UnexpectedException(),
            row.get("PAYMENT_DESCRIPTION", String::class.java) ?: throw UnexpectedException(),
            row.get("PAYMENT_AMOUNT", Number::class.java)?.toInt() ?: throw UnexpectedException(),
            row.get("PAYMENT_DATE", LocalDate::class.java) ?: throw UnexpectedException()
        )
    }
}