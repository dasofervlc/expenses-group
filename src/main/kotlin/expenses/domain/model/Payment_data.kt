package expenses.domain.model

import org.springframework.data.annotation.Id
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDate

@Table("expensesdb.PAYMENTS")
data class Payment_data(
    @Id
    @Column(value = "PAYMENT_ID")
    val paymentId: Int,

    @Column(value = "USER_NAME")
    val name: String,

    @Column(value = "PAYMENT_DESCRIPTION")
    val paymentDescription: String,

    @Column(value = "PAYMENT_AMOUNT")
    val paymentAmount: Int,

    @Column(value = "PAYMENT_DATE")
    val paymentDate: LocalDate



): Persistable<Int> {

    override fun getId(): Int {
        return paymentId
    }

    override fun isNew(): Boolean {
        return true
    }
}