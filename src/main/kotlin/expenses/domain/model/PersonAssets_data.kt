package expenses.domain.model


import org.springframework.data.annotation.Id
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table


@Table("expensesdb.BALANCES")
class PersonAssets_data(
    @Id
    @Column(value = "USER_NAME")
    val userName: String,

    @Column(value = "PERSONAL_BALANCE")
    val personalBalance: Int

) : Persistable<String> {

    override fun getId(): String {
        return userName
    }

    override fun isNew(): Boolean {
        return true
    }
}