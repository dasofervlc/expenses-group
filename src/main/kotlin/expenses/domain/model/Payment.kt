package expenses.domain.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.sql.Date

@JsonIgnoreProperties(ignoreUnknown = true)
data class Payment(
    val name: String,
    val description: String,
    val payment: Int,
    val date: Date
)