package expenses.domain.port.infrastructure

import expenses.domain.model.Payment_data
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.r2dbc.core.DatabaseClient
import reactor.core.publisher.Mono

const val insertPayment = """
    INSERT INTO PAYMENTS (code, description) VALUES (:code, :description)
"""

const val updatePayment = """
    UPDATE PAYMENTS SET code = :code, description = :description WHERE id = :productId
"""

const val deletePayment = """
    DELETE FROM PAYMENTS WHERE product_id = $1
"""
const val getSerial = """
    SELECT currval(pg_get_serial_sequence('PAYMENTS', 'id'));
"""

interface TransactionsRepository : ReactiveCrudRepository<Payment_data, String>,CustomTransactionRepository {}


interface CustomTransactionRepository {
    fun save(paymentRow: Payment_data): Mono<Payment_data>
    fun update(paymentRow: Payment_data): Mono<Int>
    fun deletePayment(paymentId: Int): Mono<Int>
    fun getSerial():Mono<Int>

}
class CustomTransactionRepositoryImpl(
    private val databaseClient: DatabaseClient
): CustomTransactionRepository {

    override fun save(paymentRow: Payment_data): Mono<Payment_data> {
        return databaseClient.sql(insertPayment)
            .filter { statement, _ -> statement.returnGeneratedValues("id").execute() }
            .bind("name", paymentRow.name)
            .bind("description", paymentRow.paymentDescription)
            .bind("amount", paymentRow.paymentAmount)
            .bind("date",paymentRow.paymentDate)
            .fetch()
            .first()
            .map { paymentRow.copy(paymentId = it["id"] as Int) }
    }

    override fun update(paymentRow: Payment_data): Mono<Int> {
        return databaseClient.sql(updatePayment)
            .bind("name", paymentRow.name)
            .bind("description", paymentRow.paymentDescription)
            .bind("amount", paymentRow.paymentAmount)
            .bind("date", paymentRow.paymentDate)
            .bind("paymentId", paymentRow.id)
            .fetch().rowsUpdated()
    }

    override fun deletePayment(paymentId: Int): Mono<Int> {
        return databaseClient
            .sql(deletePayment)
            .bind("paymentId", paymentId)
            .fetch().rowsUpdated()
    }
    override fun getSerial():Mono<Int>{
        return databaseClient
            .sql(getSerial)
            .fetch().rowsUpdated()
    }
}