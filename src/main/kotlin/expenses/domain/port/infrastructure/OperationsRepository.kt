package expenses.domain.port.infrastructure

import expenses.domain.model.Payment_data
import expenses.domain.model.PersonAssets_data
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import reactor.core.publisher.Mono

interface OperationsRepository : ReactiveCrudRepository<Payment_data, String> {

    @Query("select SUM(PERSONAL_BALANCE) from BALANCES;")
    fun getTotalBalance(): Mono<List<PersonAssets_data>>
}