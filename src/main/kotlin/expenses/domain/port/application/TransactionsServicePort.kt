package expenses.domain.port.application

import expenses.domain.model.Payment
import expenses.domain.model.Payment_data
import reactor.core.publisher.Mono


interface TransactionsServicePort {

    fun addPayment(payment: Payment): Mono<Payment_data>
}