package expenses.domain.port.application

import expenses.domain.model.PersonAssets_data
import reactor.core.publisher.Mono


interface OperationsServicePort {

    fun getTotalBalance(): Mono<List<PersonAssets_data>>
}