package expenses.domain


import expenses.domain.exceptions.ResourceNotFoundException
import expenses.domain.model.PersonAssets_data
import expenses.domain.port.infrastructure.OperationsRepository
import expenses.domain.services.OperationsServiceImpl
import org.junit.Test
import org.mockito.Mockito
import reactor.core.publisher.Mono
import reactor.test.StepVerifier

class OperationsServiceImplTest {

    private val mockOperationsRepository: OperationsRepository = Mockito.mock(OperationsRepository::class.java)
    private val operationsServiceImpl: OperationsServiceImpl = OperationsServiceImpl(mockOperationsRepository)

    @Test
    fun givenInputCallWhenIsReceivedThenRetrieveBalances() {


        val person1 = PersonAssets_data("Pepe",  50)
        val person2 = PersonAssets_data("Jose",  200)
        val returnListPersonBalances = listOf<PersonAssets_data>(person1,person2)
        Mockito.`when`(mockOperationsRepository.getTotalBalance()).thenReturn(Mono.just(returnListPersonBalances))

        //WHEN
        val response = operationsServiceImpl.getTotalBalance()

        //THEN
        StepVerifier.create(response).expectNext(returnListPersonBalances).verifyComplete()


    }

    @Test
    fun givenInputBalanceCallWhenIsReceivedThenExceptionIsRaised() {


        val expectedException= ResourceNotFoundException("Error")
        Mockito.`when`(mockOperationsRepository.getTotalBalance()).thenReturn(Mono.error(expectedException))

        //WHEN
        val response = operationsServiceImpl.getTotalBalance()

        //THEN
        StepVerifier.create(response).expectError().verify()


    }
}