package expenses.domain


import expenses.domain.exceptions.UnexpectedException
import expenses.domain.model.Payment
import expenses.domain.model.Payment_data
import expenses.domain.port.infrastructure.TransactionsRepository
import expenses.domain.services.TransactionsServiceImpl
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.sql.Date
import java.time.LocalDate


class TransactionsServiceImplTest {

    private val mockTransactionsRepository: TransactionsRepository = Mockito.mock(TransactionsRepository::class.java)
    private val transactionsServiceImpl: TransactionsServiceImpl = TransactionsServiceImpl(mockTransactionsRepository)

    @Test
    fun givenInputPaymentCallWhenIsReceivedThenPaymentIsPersisted() {

        //GIVEN
        val payment = Payment("Pepe", "primer pago", 50, Date(2022 - 9 - 16))
        val returnPayment = Payment_data(1, "Pepe", "primer pago", 50, LocalDate.of(2022, 9, 16))
        Mockito.`when`(mockTransactionsRepository.getSerial()).thenReturn(Mono.just(1))
        Mockito.`when`(mockTransactionsRepository.save(any())).thenReturn(Mono.just(returnPayment))


        //WHEN
        val response = transactionsServiceImpl.addPayment(payment)

        //THEN
        StepVerifier.create(response).expectNext(returnPayment).verifyComplete()


    }

    @Test
    fun givenInputPaymentWhenIsReceivedThenThrowsAnError() {

        //GIVEN
        val exceptionReturned = UnexpectedException("Error")
        val payment = Payment("Pepe", "primer pago", 50, Date(2022 - 9 - 16))
        Mockito.`when`(mockTransactionsRepository.getSerial()).thenReturn(Mono.just(1))
        Mockito.`when`(mockTransactionsRepository.save(any())).thenReturn(Mono.error(exceptionReturned))
        //WHEN
        val response = transactionsServiceImpl.addPayment(payment)

        //THEN
        StepVerifier.create(response).expectError().verify()


    }
    @Test
    fun givenInputPaymentWhenSerialIsRequestedThenThrowsAnError() {

        //GIVEN
        val exceptionReturned = UnexpectedException("Error")

        Mockito.`when`(mockTransactionsRepository.getSerial()).thenReturn(Mono.error(exceptionReturned))

        //WHEN
        val response = transactionsServiceImpl.getSerial()

        //THEN
        StepVerifier.create(response).expectError().verify()


    }
}