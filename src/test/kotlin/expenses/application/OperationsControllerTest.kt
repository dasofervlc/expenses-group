package expenses.application

import expenses.application.controller.OperationsController
import expenses.domain.exceptions.ResourceNotFoundException
import expenses.domain.model.PersonAssets_data
import expenses.domain.port.application.OperationsServicePort
import org.junit.Test
import org.mockito.Mockito
import reactor.core.publisher.Mono
import reactor.test.StepVerifier


class OperationsControllerTest {

    private val mockExpensesServicePortService: OperationsServicePort = Mockito.mock(OperationsServicePort::class.java)
    private val operationsController: OperationsController = OperationsController(mockExpensesServicePortService)

    @Test
    fun givenInputBalanceRequestWhenIsReceivedThenRetrieveBalances() {


        val person1 = PersonAssets_data("Pepe",  50)
        val person2 = PersonAssets_data("Jose",  200)
        val returnListPersonBalances = listOf<PersonAssets_data>(person1,person2)
        Mockito.`when`(mockExpensesServicePortService.getTotalBalance()).thenReturn(Mono.just(returnListPersonBalances))

        //WHEN
        val response = operationsController.getExpensesGroup()

        //THEN
        StepVerifier.create(response).expectNext(returnListPersonBalances).verifyComplete()


    }

    @Test
    fun givenInputBalanceRequestWhenIsReceivedThenExceptionIsRaised() {


        val expectedException= ResourceNotFoundException("Error")
        Mockito.`when`(mockExpensesServicePortService.getTotalBalance()).thenReturn(Mono.error(expectedException))

        //WHEN
        val response = operationsController.getExpensesGroup()

        //THEN
        StepVerifier.create(response).expectError().verify()


    }
}

