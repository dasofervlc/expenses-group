package expenses.application

import expenses.application.controller.TransactionsController
import expenses.domain.exceptions.UnexpectedException
import expenses.domain.model.Payment
import expenses.domain.model.Payment_data
import expenses.domain.port.application.TransactionsServicePort
import org.junit.Test
import org.mockito.Mockito
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.sql.Date
import java.time.LocalDate


class TransactionsControllerTest {

    private val mockExpensesServicePortService: TransactionsServicePort = Mockito.mock(TransactionsServicePort::class.java)
    private val transactionsController: TransactionsController = TransactionsController(mockExpensesServicePortService)

    @Test
    fun givenInputPaymentWhenIsReceivedThenCallsToSaveLogic() {

        //GIVEN
        val payment = Payment("Pepe", "primer pago", 50, Date(2022 - 9 - 16))
        val returnPayment = Payment_data(1, "Pepe", "primer pago", 50, LocalDate.of(2022, 9, 16))
        Mockito.`when`(mockExpensesServicePortService.addPayment(payment)).thenReturn(Mono.just(returnPayment))

        //WHEN
        val response = transactionsController.addPayment(payment)

        //THEN
        StepVerifier.create(response).expectNext(returnPayment).verifyComplete()


    }

    @Test
    fun givenInputPaymentWhenIsReceivedThenThrowsAnError() {

        //GIVEN
        val exceptionReturned = UnexpectedException("Error")
        val payment = Payment("Pepe", "primer pago", 50, Date(2022 - 9 - 16))
        Mockito.`when`(mockExpensesServicePortService.addPayment(payment)).thenReturn(Mono.error(exceptionReturned))
        //WHEN
        val response = transactionsController.addPayment(payment)

        //THEN
        StepVerifier.create(response).expectError().verify()


    }
}

