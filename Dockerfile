FROM maven:3-openjdk-11 AS build
COPY . /home/maven/src
WORKDIR /home/maven/src
RUN mvn package

FROM openjdk:11 AS package
EXPOSE 8080:8080
RUN mkdir /app
COPY --from=build /home/maven/src/target/*.jar /app/app.jar
CMD ["java", "-jar", "/app/app.jar"]
#ENTRYPOINT ["java","-jar","/app/app.jar"]
